package sk.fiit.hasto.gestures.preprocessing;

import sk.fiit.hasto.gestures.Gesture;

public final class XAccelerationFlipper implements GesturePreprocessor {

	@Override
	public Gesture apply(Gesture input) {
		double[][] data = input.getData();
		
		for (double[] row : data) {
			row[0] = -row[0];
		}
		
		return input.copyWithChangedData(data);
	}
	
}
