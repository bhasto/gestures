package sk.fiit.hasto.gestures.experiments;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import sk.fiit.hasto.gestures.Gesture;
import sk.fiit.hasto.gestures.da.GestureDao;
import sk.fiit.hasto.gestures.da.JdbcGestureDao;
import sk.fiit.hasto.gestures.preprocessing.CompoundGesturePreprocessor;
import sk.fiit.hasto.gestures.preprocessing.GesturePreprocessor;
import sk.fiit.hasto.gestures.preprocessing.OrientationRemover;
import sk.fiit.hasto.gestures.preprocessing.SlidingAverageCompressor;
import sk.fiit.hasto.gestures.preprocessing.XAccelerationFlipper;
import sk.fiit.hasto.gestures.similarity.DtwDistanceComputer;
import sk.fiit.hasto.utils.sql.JdbcResources;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;

public class Experiments {
	
	// database connection
	private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/gestures";
	private static final String JDBC_USER = "postgres";
	private static final String JDBC_PASSWORD = "heslo";
	
	// preprocessors prepared to use
	private static final GesturePreprocessor COMPRESSOR = new SlidingAverageCompressor(2, 1.2f);
	private static final GesturePreprocessor ORIENTATION_REMOVER = new OrientationRemover();
	private static final GesturePreprocessor X_ACCELERATION_FLIPPER = new XAccelerationFlipper();
	private static final GesturePreprocessor ONLY_ACCELEROMETER = new CompoundGesturePreprocessor(ORIENTATION_REMOVER, COMPRESSOR);
	private static final GesturePreprocessor ONLY_ACCELEROMETER_FLIPPED_X = new CompoundGesturePreprocessor(ORIENTATION_REMOVER, COMPRESSOR, X_ACCELERATION_FLIPPER);
	private static final GesturePreprocessor ACCELEROMETER_AND_ORIENTATION = COMPRESSOR;
	private static final GesturePreprocessor ACCELEROMETER_AND_ORIENTATION_FLIPPED_X = new CompoundGesturePreprocessor(COMPRESSOR, X_ACCELERATION_FLIPPER);

	private static final DtwDistanceComputer DISTANCE_COMPUTER = new DtwDistanceComputer();


	public static void main(String[] args) throws Exception {
		Connection connection = startConnection();
		GestureDao dao = new JdbcGestureDao(connection, new ObjectMapper());

		System.out.println("Comparing variant A - left to variant D - right");
		
		// load two groups of gestures from database
		List<Gesture> aLeft = dao.getByNote("AL");
		List<Gesture> aRight = dao.getByNote("DR");
		
		// compare each gesture from left group to each gesture in right group
		System.out.println("Using both data from accelerometer and orientation");
		compareEachToEach(aLeft, ACCELEROMETER_AND_ORIENTATION, aRight, ACCELEROMETER_AND_ORIENTATION_FLIPPED_X);
		System.out.println();
		System.out.println();
		
		// compare only corresponding gestures, left1 to right1, left2 to right2, etc.
		System.out.println("Using both data from accelerometer and orientation");
		compareCorresponding(aLeft, ACCELEROMETER_AND_ORIENTATION, aRight, ACCELEROMETER_AND_ORIENTATION_FLIPPED_X);
		System.out.println();
		System.out.println();
		
		// compare each gesture from left group to each gesture in right group
		System.out.println("Using only data from accelerometer");
		compareEachToEach(aLeft, ONLY_ACCELEROMETER, aRight, ONLY_ACCELEROMETER_FLIPPED_X);
		System.out.println();
		System.out.println();
		
		// compare only corresponding gestures, left1 to right1, left2 to right2, etc.
		System.out.println("Using only data from accelerometer");
		compareCorresponding(aLeft, ONLY_ACCELEROMETER, aRight, ONLY_ACCELEROMETER_FLIPPED_X);
		
		
		JdbcResources.close(connection);
	}
	
	
	private static void compareTwoGestures(Gesture left, Gesture right) {
		System.out.println("Distance: " + computeGesturesDistance(left, right));
	}
	
	private static void compareEachToEach(List<Gesture> leftDataset, GesturePreprocessor leftPreprocessor, List<Gesture> rightDataset, GesturePreprocessor rightPreprocessor) {
		System.out.println("Comparing each to each");
		List<Double> values = Lists.newArrayListWithCapacity(leftDataset.size() * rightDataset.size());
		
		for (int i = 0; i < leftDataset.size(); i++) {
			Gesture leftGesture = leftPreprocessor.apply(leftDataset.get(i));
			
			for (int j = 0; j < rightDataset.size(); j++) {
				Gesture rightGesture = rightPreprocessor.apply(rightDataset.get(j));
				final double distance = computeGesturesDistance(leftGesture, rightGesture);
				System.out.print("Distance of #" + i + " in left dataset and #" + j + " in right dataset: ");
				System.out.println(distance);
				values.add(distance);
			}
		}
		
		printAverageAndDeviation(values);
	}
	
	private static void compareCorresponding(List<Gesture> leftDataset, GesturePreprocessor leftPreprocessor, List<Gesture> rightDataset, GesturePreprocessor rightPreprocessor) {
		System.out.println("Comparing corresponding");

		assert leftDataset.size() == rightDataset.size();
		List<Double> values = Lists.newArrayListWithCapacity(leftDataset.size());
		for (int i = 0; i < leftDataset.size(); i++) {
			Gesture leftGesture = leftPreprocessor.apply(leftDataset.get(i));
			Gesture rightGesture = rightPreprocessor.apply(rightDataset.get(i));
			double distance = computeGesturesDistance(leftGesture, rightGesture);
			System.out.print("Distance of #" + i + " in left dataset and #" + i + " in right dataset: ");
			System.out.println(distance);
			
			values.add(distance);
		}
		
		printAverageAndDeviation(values);
	}

	private static double computeGesturesDistance(Gesture left, Gesture right) {
		return DISTANCE_COMPUTER.compute(left, right, 100);
	}
	
	private static void printAverageAndDeviation(List<Double> values) {
		double sum = 0;
		for (double value : values) sum += value;
		final double avg = sum / values.size();
		
		double squaredDifferenceSum = 0;
		for (double value : values) squaredDifferenceSum += (value-avg) * (value-avg);
		final double variance = squaredDifferenceSum / (values.size() - 1);
		final double stdDev = Math.sqrt(variance);
		
		System.out.println("Average: " + avg + " +- " + stdDev); 
	}
	
	private static Connection startConnection() throws SQLException {
		org.postgresql.Driver.class.getClass();
		return DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASSWORD);
	}

}
