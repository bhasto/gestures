package sk.fiit.hasto.gestures.similarity;

import static java.lang.Math.abs;
import static java.lang.Math.max;
import static java.lang.Math.min;

import java.util.List;

import org.ejml.data.DenseMatrix64F;
import org.ejml.ops.CommonOps;

import sk.fiit.hasto.gestures.Gesture;

import com.google.common.collect.Lists;
import com.google.common.primitives.Doubles;

public final class DtwDistanceComputer {

	public double compute(Gesture gesture1, Gesture gesture2, int window) {
		return compute(gesture1.getData(), gesture2.getData(), window);
	}
	
	private double compute(double[][] signal1, double[][] signal2, int window) {
		DenseMatrix64F[] signal1Vectors = CommonOps.rowsToVector(new DenseMatrix64F(signal1), null);
		DenseMatrix64F[] signal2Vectors = CommonOps.rowsToVector(new DenseMatrix64F(signal2), null);
		DenseMatrix64F dtw = dtwMatrix(signal1Vectors, signal2Vectors, window);
		return normalizedDtwDistance(dtw);
	}
	
	private static DenseMatrix64F dtwMatrix(DenseMatrix64F[] sig1, DenseMatrix64F[] sig2, int window) {
		DenseMatrix64F dtw = initDtwMatrix(sig1.length, sig2.length);
		
		window = max(window, abs(sig1.length - sig2.length));
		for (int row = 1; row <= sig1.length; row++) {
			for (int col = max(1, row-window); col <= min(sig2.length, row+window); col++) {
				double neighbour1Cost = dtw.get(row, col-1);
				double neighbour2Cost = dtw.get(row-1, col);
				double neighbour3Cost = dtw.get(row-1, col-1);
				double cost = vectorDistance(sig1[row-1], sig2[col-1]);
				dtw.set(row, col, cost + Doubles.min(neighbour1Cost, neighbour2Cost, neighbour3Cost));
			}
		}

		return CommonOps.extract(dtw, 1, dtw.numRows, 1, dtw.numCols);
	}
	
	private static double normalizedDtwDistance(DenseMatrix64F dtw) {
		List<double[]> path = optimalDtwPath(dtw);
		return dtw.get(dtw.getNumElements() - 1) / path.size();
	}

	private static List<double[]> optimalDtwPath(DenseMatrix64F dtw) {
		List<double[]> path = Lists.newArrayList();
		for (int x=0, y=0; y < dtw.numRows && x < dtw.numCols;) {
			path.add(new double[] {y, x});
			
			// candidate 1
			int nextY = y+1;
			int nextX = x+1;
			
			// candidate 2
			if (safeGet(dtw, y, x+1) < safeGet(dtw, nextY, nextX)) {
				nextY = y;
			}
			
			// candidate 3
			if (safeGet(dtw, y+1, x) < safeGet(dtw, nextY, nextX)) {
				nextY = y+1;
				nextX = x;
			}
			
			y = nextY;
			x = nextX;
		}
		
		return path;
	}
	
	private static double safeGet(DenseMatrix64F dtw, int row, int col) {
		if (row < 0 || row >= dtw.numRows) return Double.POSITIVE_INFINITY;
		if (col < 0 || col >= dtw.numCols) return Double.POSITIVE_INFINITY;
		return dtw.get(row, col);
	}

	private static DenseMatrix64F initDtwMatrix(int rows, int cols) {
		DenseMatrix64F dtw = new DenseMatrix64F(rows + 1, cols + 1);
		CommonOps.set(dtw, Double.POSITIVE_INFINITY);
		dtw.set(0, 0, 0);
		return dtw;
	}

	private static double vectorDistance(DenseMatrix64F vector1, DenseMatrix64F vector2) {
		DenseMatrix64F subtraction = new DenseMatrix64F(vector1.numRows, vector1.numCols);
		CommonOps.sub(vector1, vector2, subtraction);
		return CommonOps.elementSumAbs(subtraction);
	}

}
