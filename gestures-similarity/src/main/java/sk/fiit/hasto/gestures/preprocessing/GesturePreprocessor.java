package sk.fiit.hasto.gestures.preprocessing;

import sk.fiit.hasto.gestures.Gesture;


public interface GesturePreprocessor {
	
	Gesture apply(Gesture input);

}
