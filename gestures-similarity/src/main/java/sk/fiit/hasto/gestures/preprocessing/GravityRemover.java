package sk.fiit.hasto.gestures.preprocessing;

import static java.lang.Math.cos;
import static java.lang.Math.sin;
import sk.fiit.hasto.gestures.Gesture;

public final class GravityRemover implements GesturePreprocessor {

	private static final double G = 9.80665;
	
	@Override
	public Gesture apply(Gesture input) {
		double[][] data = input.getData();
		
		for (double[] row : data) {
			double[] gravity = { 
				-G * sin(row[5]) * cos(row[4]),
				-G * sin(row[4]),
				 G * cos(row[4]) * cos(row[5])
			};
			
			row[0] -= gravity[0];
			row[1] -= gravity[1];
			row[2] -= gravity[2];
		}
		
		return input.copyWithChangedData(data);
	}
	
}
