package sk.fiit.hasto.gestures.preprocessing;

import static java.lang.Math.ceil;
import static java.lang.Math.min;

import java.util.List;

import org.ejml.data.DenseMatrix64F;
import org.ejml.ops.CommonOps;

import sk.fiit.hasto.gestures.Gesture;

import com.google.common.collect.Lists;

public final class SlidingAverageCompressor implements GesturePreprocessor {

	private final float windowSize;
	private final float stepSize;

	public SlidingAverageCompressor(float windowSize, float stepSize) {
		this.windowSize = windowSize;
		this.stepSize = stepSize;
	}

	@Override
	public Gesture apply(Gesture input) {
		double[][] data = compress(input.getData());
		return input.copyWithChangedData(data);
	}

	private double[][] compress(double[][] data) {
		List<double[]> compressed = prepareCompressedData(data);
		
		DenseMatrix64F matrix = new DenseMatrix64F(data);
		for (float from = 0; ceil(from) < matrix.numRows; from += stepSize) {
			DenseMatrix64F window = getWindow(matrix, from);
			DenseMatrix64F avg = averageWindow(window);
			compressed.add(avg.data);
		}
		
		return compressed.toArray(new double[compressed.size()][]);
	}

	private List<double[]> prepareCompressedData(double[][] uncompressed) {
		int length = (int) (uncompressed.length / stepSize);
		return Lists.newArrayListWithExpectedSize(length);
	}

	private DenseMatrix64F getWindow(DenseMatrix64F data, float from) {
		double to = from + windowSize;
		int fromIndex = (int) ceil(from);
		int toIndex = min((int) ceil(to), data.numRows);
		return CommonOps.extract(data, fromIndex, toIndex, 0, data.numCols);
	}

	private static DenseMatrix64F averageWindow(DenseMatrix64F window) {
		DenseMatrix64F avg = CommonOps.sumCols(window, null);
		CommonOps.divide(window.numRows, avg);
		return avg;
	}

}
