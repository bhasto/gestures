package sk.fiit.hasto.gestures.preprocessing;

import sk.fiit.hasto.gestures.Gesture;

public final class OrientationRemover implements GesturePreprocessor {

	@Override
	public Gesture apply(Gesture input) {
		double[][] data = input.getData();
		
		for (double[] row : data) {
			row[3] = row[4] = row[5] = 0;
		}
		
		return input.copyWithChangedData(data);
	}
	
}
