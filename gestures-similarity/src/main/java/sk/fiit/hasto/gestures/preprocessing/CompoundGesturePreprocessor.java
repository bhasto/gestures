package sk.fiit.hasto.gestures.preprocessing;

import java.util.List;

import sk.fiit.hasto.gestures.Gesture;

import com.google.common.collect.ImmutableList;

public final class CompoundGesturePreprocessor implements GesturePreprocessor {

	private final List<GesturePreprocessor> preprocessors;
	
	public CompoundGesturePreprocessor(GesturePreprocessor... preprocessors) {
		this.preprocessors = ImmutableList.copyOf(preprocessors);
	}

	public Gesture apply(Gesture input) {
		Gesture preprocessed = input;
		for (GesturePreprocessor preprocessor : preprocessors) {
			preprocessed = preprocessor.apply(preprocessed);
		}
		
		return preprocessed;
	}
	
}
