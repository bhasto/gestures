package sk.fiit.hasto.utils.arrays;

import javax.annotation.Nullable;

public final class Arrays2D {

	private Arrays2D() {}
	
	/**
	 * @param array
	 * @return {@code null} if array is {@code null}, deep clone otherwise 
	 */
	public static @Nullable double[][] deepCopy(@Nullable double[][] array) {
		if (array == null) {
			return null;
		}
		
		double[][] copy = new double[array.length][];
		for (int row = 0; row < array.length; row++) {
			copy[row] = (array[row] == null) ? null : array[row].clone();
		}
		
		return copy;
	}
	
}
