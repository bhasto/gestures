package sk.fiit.hasto.gestures;
import java.util.Arrays;

import javax.annotation.concurrent.Immutable;

import sk.fiit.hasto.utils.arrays.Arrays2D;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

@Immutable
public final class Gesture {

	private final double[][] data;
	private final String username;
	
	@JsonCreator
	public Gesture(@JsonProperty("data") double[][] data, @JsonProperty("username") String username) {
		this.data = Arrays2D.deepCopy(data);
		this.username = username;
	}
	
	public Gesture copyWithChangedData(double[][] data) {
		return new Gesture(data, username);
	}
	
	public double[][] getData() {
		return Arrays2D.deepCopy(data);
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this)
				.add("data", Arrays.deepToString(data))
				.add("username", username)
				.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Gesture)) {
			return false;
		}
		
		Gesture other = (Gesture) obj;
		return username.equals(other.username) && Arrays.deepEquals(data, other.data);
	}
	
	@Override
	public int hashCode() {
		return Objects.hashCode(username, data);
	}
	
}
