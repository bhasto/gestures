package sk.fiit.hasto.gestures.server;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sk.fiit.hasto.gestures.da.GestureDao;
import sk.fiit.hasto.gestures.da.JdbcGestureDao;
import sk.fiit.hasto.utils.sql.JdbcResources;

import com.fasterxml.jackson.databind.ObjectMapper;

public final class GestureServlet extends HttpServlet {

	private static final long serialVersionUID = -1356136067543460134L;

	private Connection connection;
	private GestureDao gestureDao;

	@Override
	public void init() throws ServletException {
		try {
			org.postgresql.Driver.class.getClass();
			ServletConfig config = getServletConfig();
			String jdbcUrl = config.getInitParameter("jdbc.url");
			String jdbcUser = config.getInitParameter("jdbc.user");
			String jdbcPassword = config.getInitParameter("jdbc.password");
			connection = DriverManager.getConnection(jdbcUrl, jdbcUser, jdbcPassword);
			gestureDao = new JdbcGestureDao(connection, new ObjectMapper());
		} catch (SQLException e) {
			throw new ServletException(e);
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// just store gesture for later use
		String json = req.getParameter("gesture");
		gestureDao.save(json);

		resp.setStatus(HttpServletResponse.SC_OK);
	}

	@Override
	public void destroy() {
		try {
			JdbcResources.close(connection);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

}
