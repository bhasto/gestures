package sk.fiit.hasto.gestures.client;

import java.io.IOException;
import java.util.List;

import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import sk.fiit.hasto.gestures.Gesture;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;

public final class HttpGestureUploader {
	
	private final HttpClient httpClient;
	private final ObjectMapper mapper;
	
	public HttpGestureUploader(HttpClient httpClient, ObjectMapper mapper) {
		this.httpClient = httpClient;
		this.mapper = mapper;
	}

	public void upload(String url, Gesture gesture) throws IOException {
		HttpPost request = new HttpPost(url);
		List<BasicNameValuePair> params = createParametersList(gesture);
		request.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
		
		httpClient.execute(request);
	}

	private List<BasicNameValuePair> createParametersList(Gesture gesture) {
		return ImmutableList.of(new BasicNameValuePair("gesture", toJson(gesture)));
	}
	
	private String toJson(Object value) {
		try {
			return mapper.writeValueAsString(value);
		} catch (IOException never) {
			throw new AssertionError(never);
		}
	}

}
