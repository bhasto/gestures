package sk.fiit.hasto.gestures.android;

import java.io.IOException;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

import sk.fiit.hasto.gestures.Gesture;
import sk.fiit.hasto.gestures.client.HttpGestureUploader;
import sk.fiit.hasto.gestures.recording.FusedSensorDataRecorder;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.hardware.SensorManager;
import android.preference.PreferenceManager;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.googlecode.androidannotations.annotations.AfterInject;
import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.OptionsItem;
import com.googlecode.androidannotations.annotations.OptionsMenu;
import com.googlecode.androidannotations.annotations.SystemService;
import com.googlecode.androidannotations.annotations.UiThread;
import com.googlecode.androidannotations.annotations.ViewById;

@EActivity(R.layout.main)
@OptionsMenu(R.menu.menu)
public class MainActivity extends Activity implements OnCheckedChangeListener, OnSharedPreferenceChangeListener {
	
	@ViewById TextView usernameText;
	@ViewById ToggleButton startStopButton;
	@SystemService SensorManager sensorManager;
	
	private FusedSensorDataRecorder fusedSensorDataRecorder;
	private HttpGestureUploader httpGestureUploader;
	private SharedPreferences preferences;
	
	@AfterInject
	void initDependencies() {
		PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
		preferences = PreferenceManager.getDefaultSharedPreferences(this);
		preferences.registerOnSharedPreferenceChangeListener(this);
		
		fusedSensorDataRecorder = new FusedSensorDataRecorder(sensorManager, false);
		
		HttpClient httpClient = new DefaultHttpClient();
		httpGestureUploader = new HttpGestureUploader(httpClient, new ObjectMapper());
	}

	@OptionsItem
	void prefsItemSelected() {
		startActivity(new Intent(this, PreferencesActivity.class));
	}
	
	@UiThread
	@AfterViews
	void updateUsernameTextView() {
		usernameText.setText(getUsername());
	}

	@AfterViews
	void registerStartStopButtonListener() {
		startStopButton.setOnCheckedChangeListener(this);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		updateUsernameTextView();
	}
	
	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		try {
			if (isChecked) {
				fusedSensorDataRecorder.startRecording();
			} else {
				double[][] data = fusedSensorDataRecorder.stopRecording();
				Gesture gesture = new Gesture(data, getUsername());
				httpGestureUploader.upload(getServerUrl(), gesture);
			}
		} catch (IOException e) {
			// forced exit - problem communicating with server
			throw new RuntimeException(e);
		}
	}
	
	private String getUsername() {
		return preferences.getString(getString(R.string.username_pref_key), "");
	}
	
	private String getServerUrl() {
		return preferences.getString(getString(R.string.server_url_pref_key), "");
	}

}
