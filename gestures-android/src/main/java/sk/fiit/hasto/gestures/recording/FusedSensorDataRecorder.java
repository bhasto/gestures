package sk.fiit.hasto.gestures.recording;

import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.toRadians;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.google.common.collect.Lists;

// TODO: this class has way too much responsibility
public class FusedSensorDataRecorder implements SensorEventListener {
	
	private static final float NS2S = 0.000000001f;
	private static final float COMPOUND_FILTER_COEFFICIENT = 0.98f;
	private static final int SENSOR_FUSION_PERIOD_MS = 25;
	
	private final SensorManager sensorManager;
	private final Sensor accelerometer;
	private final Sensor compass;
	private final Sensor gyroscope;
	private final boolean correctGyroValues;
	
	private float[] acc = new float[3];
	private float[] mag = new float[3];
	private float[] accMagRotation = new float[9];
	private boolean accMagInitialized;

	private long lastGyroTimestamp;
	private float[] gyro = new float[3];
	private float[] gyroRotation = new float[9];
	private boolean gyroInitialized;

	private Timer scheduler;
	private float[] fusedOrientation = new float[3];

	private final List<double[]> recording;

	
	public FusedSensorDataRecorder(SensorManager sensorManager, boolean correctGyroValues) {
		this.sensorManager = sensorManager;
		this.accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		this.compass = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
		this.gyroscope = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
		this.correctGyroValues = correctGyroValues;
		this.recording = Lists.newArrayListWithExpectedSize(150);
	}

	public void startRecording() {
		recording.clear();
		
		accMagInitialized = false;
		gyroInitialized = false;
		lastGyroTimestamp = 0;
		
		sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_FASTEST);
		sensorManager.registerListener(this, compass, SensorManager.SENSOR_DELAY_FASTEST);
		sensorManager.registerListener(this, gyroscope, SensorManager.SENSOR_DELAY_FASTEST);
		
		scheduler = new Timer();
		scheduler.scheduleAtFixedRate(new ComputeFusedOrientationTask(), 30, SENSOR_FUSION_PERIOD_MS);
	}
	
	/**
	 * @return time series of sensor data
	 */
	public double[][] stopRecording() {
		sensorManager.unregisterListener(this);
		scheduler.cancel();
		return recording.toArray(new double[recording.size()][]);
	}
	
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// not interesting
	}
	
	@Override
	public void onSensorChanged(SensorEvent event) {
		switch (event.sensor.getType()) {
		case Sensor.TYPE_ACCELEROMETER:
			Matrices.copy(event.values, acc);
			computeAccMagRotation();
			break;
			
		case Sensor.TYPE_MAGNETIC_FIELD:
			Matrices.copy(event.values, mag);
			break;

		case Sensor.TYPE_GYROSCOPE:
			Matrices.copy(event.values, gyro);
			if (correctGyroValues) correctGyroValues(gyro);
			computeGyroRotation(event.timestamp);
			break;
		}
	}

	/**
	 * Computes device rotation based on values from accelerometer and magnetometer.
	 * Should be called after receiving sensor event from accelerometer or magnetometer.
	 */
	private void computeAccMagRotation() {
		if (SensorManager.getRotationMatrix(accMagRotation, null, acc, mag)) {
			accMagInitialized = true;
		}
	}
	
	/**
	 * Computes device rotation based on values from gyroscope.
	 * Should be called after receiving sensor event from gyroscope.
	 * @param gyroEventTimestamp time when gyroscope event was received
	 */
	private void computeGyroRotation(long gyroEventTimestamp) {
		if (accMagInitialized && !gyroInitialized) {
			Matrices.copy(accMagRotation, gyroRotation);
			gyroInitialized = true;
		}
		
		if (gyroInitialized && lastGyroTimestamp != 0) {
			final float dT = (gyroEventTimestamp - lastGyroTimestamp) * NS2S;
			float[] deltaRotationVector = computeDeltaRotationVector(gyro, dT);
			float[] deltaRotationMatrix = Matrices.rotationVectorToRotationMatrix(deltaRotationVector);
			gyroRotation = Matrices.multiplyMatrices(gyroRotation, deltaRotationMatrix);
		}
		
		lastGyroTimestamp = gyroEventTimestamp;
	}
	
	/**
	 * Some devices return gyro values in deg/s instead of rad/s.
	 * This function converts those values to rad/s. 
	 * @param gyroValues values in deg/s, modified to rad/s
	 */
	private static void correctGyroValues(float[] gyroValues) {
		gyroValues[0] = (float) toRadians(gyroValues[0]);
		gyroValues[1] = (float) toRadians(gyroValues[1]);
		gyroValues[2] = (float) toRadians(gyroValues[2]);
	}

	private void recordObservation() {
		double[] observation = {
			acc[0], acc[1], acc[2],
			fusedOrientation[0], fusedOrientation[1], fusedOrientation[2]
		};
		
		recording.add(observation);
	}
    
    /**
	 * Code based on Android documentation.
	 * @param angSpeeds angular speeds - from gyroscope (not modified)
	 * @param dT time delta
	 * @return rotation vector when object is rotating with 
	 * given angular speeds during given time interval dT.
	 */
	public static float[] computeDeltaRotationVector(float[] angSpeeds, float dT) {
		// normalize vector if needed
		float[] norm = angSpeeds.clone();
		float omegaMagnitude = Matrices.computeVectorMagnitude(norm);
		if (omegaMagnitude > 0.000000001f) Matrices.normalizeVector(norm);
		
		float thetaOverTwo = dT * omegaMagnitude / 2f;
		float sinThetaOverTwo = (float) sin(thetaOverTwo);
		float cosThetaOverTwo = (float) cos(thetaOverTwo);
		return new float[] {
			sinThetaOverTwo * norm[0], 
			sinThetaOverTwo * norm[1], 
			sinThetaOverTwo * norm[2], 
			cosThetaOverTwo
		};
	}

	private class ComputeFusedOrientationTask extends TimerTask {
        public void run() {
        	if (!accMagInitialized || !gyroInitialized) return;
        	
        	float[] gyroOrientation = Matrices.rotationToOrientation(gyroRotation);
        	float[] accMagOrientation = Matrices.rotationToOrientation(accMagRotation);
        	
        	float coeff = COMPOUND_FILTER_COEFFICIENT;
            fusedOrientation[0] = coeff * gyroOrientation[0] + (1-coeff) * accMagOrientation[0];
            fusedOrientation[1] = coeff * gyroOrientation[1] + (1-coeff) * accMagOrientation[1];
            fusedOrientation[2] = coeff * gyroOrientation[2] + (1-coeff) * accMagOrientation[2];
            
            gyroRotation = Matrices.orientationToRotation(fusedOrientation);
			recordObservation();
        }
    }


}
	