package sk.fiit.hasto.gestures.android;

import android.os.Bundle;
import android.preference.PreferenceActivity;

public final class PreferencesActivity extends PreferenceActivity { 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
	}
	
}

