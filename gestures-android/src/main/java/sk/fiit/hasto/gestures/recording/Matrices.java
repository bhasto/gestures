package sk.fiit.hasto.gestures.recording;

import static java.lang.Math.cos;
import static java.lang.Math.sin;
import android.hardware.SensorManager;
import android.opengl.Matrix;

final class Matrices {
	
	private Matrices() {}

	/**
	 * @param rotation rotation matrix (not modified)
	 * @return orientation vector
	 */
	public static float[] rotationToOrientation(float[] rotation) {
	    return SensorManager.getOrientation(rotation, new float[3]);
	}

	/**
	 * @param orientation orientation vector (not modified)
	 * @return 4x4 rotation matrix
	 */
	public static float[] orientationToRotation(float[] orientation) {
		float sinX = (float) sin(orientation[1]);
        float cosX = (float) cos(orientation[1]);
        float sinY = (float) sin(orientation[2]);
        float cosY = (float) cos(orientation[2]);
        float sinZ = (float) sin(orientation[0]);
        float cosZ = (float) cos(orientation[0]);

        // rotation about x-axis (pitch), y-axis (roll) and z-axis (azimuth)
        float[] xM = new float[] {1f, 0f, 0f, 0f, cosX, sinX, 0f, -sinX, cosX};
        float[] yM = new float[] {cosY, 0f, sinY, 0f, 1f, 0f, -sinY, 0f, cosY};
        float[] zM = new float[] {cosZ, sinZ, 0f, -sinZ, cosZ, 0f, 0f, 0f, 1f};
        
        // rotation order is y, x, z (roll, pitch, azimuth)
        return multiplyMatrices(zM, multiplyMatrices(xM, yM));
	}

	/**
	 * Based on source code from higher Android version.
	 * @param rotationVector (not modified)
	 * @return 3x3 rotation matrix
	 */
	public static float[] rotationVectorToRotationMatrix(float[] rotationVector) {
		float q0 = rotationVector[3];
	    float q1 = rotationVector[0];
	    float q2 = rotationVector[1];
	    float q3 = rotationVector[2];
	
	    return new float[] {
			1 - 2*q2*q2 - 2*q3*q3,
			2*q1*q2 - 2*q3*q0,
			2*q1*q3 + 2*q2*q0,
			
			2*q1*q2 + 2*q3*q0,
			1 - 2*q1*q1 - 2*q3*q3,
			2*q2*q3 - 2*q1*q0,
			
			2*q1*q3 - 2*q2*q0,
			2*q2*q3 + 2*q1*q0,
			1 - 2*q1*q1 - 2*q2*q2,
	    };
	}

	/**
	 * @param A left 4x4 matrix (not modified)
	 * @param B right 4x4 matrix (not modified)
	 * @return multiplication of the matrices
	 */
	public static float[] multiplyMatrices(float[] A, float[] B) {
		return new float[] {
	    	A[0] * B[0] + A[1] * B[3] + A[2] * B[6],
	    	A[0] * B[1] + A[1] * B[4] + A[2] * B[7],
	    	A[0] * B[2] + A[1] * B[5] + A[2] * B[8],
	    	
	    	A[3] * B[0] + A[4] * B[3] + A[5] * B[6],
	    	A[3] * B[1] + A[4] * B[4] + A[5] * B[7],
	    	A[3] * B[2] + A[4] * B[5] + A[5] * B[8],
	    	
	    	A[6] * B[0] + A[7] * B[3] + A[8] * B[6],
	    	A[6] * B[1] + A[7] * B[4] + A[8] * B[7],
	    	A[6] * B[2] + A[7] * B[5] + A[8] * B[8]
		};
	}

	/**
	 * @param V vector
	 * @return vector length (magnitude)
	 */
	public static float computeVectorMagnitude(float[] V) {
		return Matrix.length(V[0], V[1], V[2]);
	}

	/**
	 * Normalizes given vector.
	 * @param V vector to normalize, modified to normalized values
	 */
	public static void normalizeVector(float[] V) {
		float magnitude = Matrices.computeVectorMagnitude(V);
		V[0] /= magnitude;
		V[1] /= magnitude;
		V[2] /= magnitude;
	}

	/**
	 * @param src source vector or matrix (not modified)
	 * @param dst destination vector or matrix (modified)
	 * @return dst
	 */
	public static float[] copy(float[] src, float[] dst) {
		assert src.length == dst.length;
		System.arraycopy(src, 0, dst, 0, src.length);
		return dst;
	}
	
	

}
