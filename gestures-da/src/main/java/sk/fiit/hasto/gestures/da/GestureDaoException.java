package sk.fiit.hasto.gestures.da;

public final class GestureDaoException extends RuntimeException {

	private static final long serialVersionUID = 3093628410018017000L;

	public GestureDaoException(Throwable cause) {
		super(cause);
	}

}
