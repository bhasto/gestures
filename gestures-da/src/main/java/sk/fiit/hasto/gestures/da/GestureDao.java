package sk.fiit.hasto.gestures.da;

import java.util.List;

import sk.fiit.hasto.gestures.Gesture;

public interface GestureDao {

	void save(String gestureJson) throws GestureDaoException;
	
	Gesture getById(int id) throws GestureDaoException;

	List<Gesture> getByNote(String note) throws GestureDaoException;


}
