package sk.fiit.hasto.gestures.da;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Nullable;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;

import sk.fiit.hasto.gestures.Gesture;
import sk.fiit.hasto.utils.sql.JdbcResources;

public final class JdbcGestureDao implements GestureDao {
	
	private final Connection connection;
	private final ObjectMapper mapper;

	public JdbcGestureDao(Connection connection, ObjectMapper mapper) {
		this.connection = connection;
		this.mapper = mapper;
	}

	@Override
	public void save(String gesture) throws GestureDaoException {
		PreparedStatement stmt = null;
		try {
			String sql = "INSERT INTO raw(gesture) VALUES (?)";
			stmt = connection.prepareStatement(sql);
			stmt.setString(1, gesture);
			stmt.executeUpdate();
		} catch (SQLException e) {
			throw new GestureDaoException(e);
		} finally {
			JdbcResources.closeQuietly(stmt);
		}
	}

	@Override
	public @Nullable Gesture getById(int id) throws GestureDaoException {
		PreparedStatement stmt = null;
		try {
			String sql = "SELECT * FROM raw WHERE id = ?";
			stmt = connection.prepareStatement(sql);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			
			if (!rs.next()) return null;
			
			String json = rs.getString("gesture");
			return mapper.readValue(json, Gesture.class);
		} catch (SQLException e) {
			throw new GestureDaoException(e);
		} catch (JsonParseException e) {
			throw new GestureDaoException(e);
		} catch (JsonMappingException e) {
			throw new GestureDaoException(e);
		} catch (IOException never) {
			throw new AssertionError(never);
		} finally {
			JdbcResources.closeQuietly(stmt);
		}
	}

	@Override
	public List<Gesture> getByNote(String note) throws GestureDaoException {
		PreparedStatement stmt = null;
		try {
			String sql = "SELECT * FROM raw WHERE note = ? ORDER BY id ASC";
			stmt = connection.prepareStatement(sql);
			stmt.setString(1, note);
			ResultSet rs = stmt.executeQuery();
			
			List<Gesture> gestures = Lists.newArrayList();
			while (rs.next()) {
				String gestureJson = rs.getString("gesture");
				Gesture gesture = mapper.readValue(gestureJson, Gesture.class);
				gestures.add(gesture);
			}
			return gestures;
		} catch (SQLException e) {
			throw new GestureDaoException(e);
		} catch (JsonParseException e) {
			throw new GestureDaoException(e);
		} catch (JsonMappingException e) {
			throw new GestureDaoException(e);
		} catch (IOException never) {
			throw new AssertionError(never);
		} finally {
			JdbcResources.closeQuietly(stmt);
		}
	}

}
