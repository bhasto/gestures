package sk.fiit.hasto.utils.sql;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.annotation.Nullable;

public final class JdbcResources {

	private JdbcResources() {}
	
	public static void close(@Nullable Connection connection) throws SQLException {
		if (connection != null) {
			connection.close();
		}
	}
	
	public static void close(@Nullable Statement statement) throws SQLException {
		if (statement != null) {
			statement.close();
		}
	}
	
	public static void closeQuietly(@Nullable Connection connection) {
		try {
			close(connection);
		} catch (SQLException ignore) {
			// ignore exception
		}
	}
	
	public static void closeQuietly(@Nullable Statement statement) {
		try {
			close(statement);
		} catch (SQLException ignore) {
			// ignore exception
		}
	}
	
}
